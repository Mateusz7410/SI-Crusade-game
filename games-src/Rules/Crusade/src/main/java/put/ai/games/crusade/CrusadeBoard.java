package put.ai.games.crusade;

import java.util.ArrayList;
import java.util.List;
import put.ai.games.game.Move;
import put.ai.games.game.Player;
import put.ai.games.game.Player.Color;
import put.ai.games.game.TypicalBoard;
import put.ai.games.game.exceptions.CheatingException;
import put.ai.games.game.exceptions.RuleViolationException;
import put.ai.games.game.moves.MoveMove;
import put.ai.games.game.moves.impl.MoveMoveImpl;

public class CrusadeBoard extends TypicalBoard {

	public List<Color[][]> previousStates = new ArrayList<>();
	public List<CrusadeMove> previousMoves = new ArrayList<>();
	
	protected int size;

	protected CrusadeMove getLastMove()
	{
		return previousMoves.get(previousMoves.size()-1);
	}
	
	protected static Color[][] cloneSquareState(Color[][] input)
	{
		int size = input.length;
		
		Color[][] newState = new Color[size][size];
        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                newState[i][j] = input[i][j];
            }
        }
		
		return newState;
	}
	
	//copying constructor, not sure what for
	public CrusadeBoard(CrusadeBoard input)
	{
		//will init new state array and copy state there
		super(input);
		for(Color[][] s : input.previousStates)
		{
			this.previousStates.add(cloneSquareState(s));
		}
		//move is immutable (does not have setters)
		for(CrusadeMove move : input.previousMoves)
		{
			this.previousMoves.add(move);
		}
	}
	
	@Override
	public CrusadeBoard clone()
	{
		return new CrusadeBoard(this);
	}
	
	
	
    public CrusadeBoard(int size) 
	{
		super(size);
		
        this.size = size;
		
        Color current = Color.PLAYER2;
       
        //TODO adjust (including balance) for higher size boards 
        for (int x = 0; x < size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                state[x][y] = current;
                current = Player.getOpponent(current);
            }
            current = Player.getOpponent(current);
        }
        //balance first player advantage
        state[size-1][0] = Color.PLAYER2;
    }

	
    @Override
    public Color getWinner(Color currentPlayer) {
        //in this game both players have no valid moves at the same time
        if (getMovesFor(currentPlayer).isEmpty()) {
            int player1Stones = countStones(Color.PLAYER1);
            int player2Stones = countStones(Color.PLAYER2);
            {
                if(player1Stones > player2Stones)
                {
                    return Color.PLAYER1;
                }
                if(player2Stones > player1Stones)
                {
                    return Color.PLAYER2;
                }
                return Color.EMPTY;
            }
        }
        return null;
    }

    //TODO why is this needed?
    @Override
    protected boolean canMove(Color color) {
        return (getMovesFor(color).size() > 1);
    }

    @Override
    public void doMove(Move move) {
        CrusadeMove currentMove = (CrusadeMove) move;
        
		previousStates.add(cloneSquareState(state));
		previousMoves.add(currentMove);
		
        //the stone moved it's at the new position
        //(there is always opponent's stone at the destination
        //(and it's always captured - overriden)
        state[currentMove.getDstX()][currentMove.getDstY()] = move.getColor();
        //the stone is not at the previous position
        state[currentMove.getSrcX()][currentMove.getSrcY()] = Color.EMPTY;
        
        //all adjacent opponent's stones change to our stones
        //-- get enemies adjacent to our new position
        for(MoveMove contagiousMove : getAdjacentOpponents(currentMove.getDstX(), currentMove.getDstY(), currentMove.getColor()))
        {
            //override enemy stone
            state[contagiousMove.getDstX()][contagiousMove.getDstY()] = currentMove.getColor();
        }
    }

    //we formulated result as list of moves
    //to use it both in getMovesFor()
    //and in move evaluation (with a different meaning)
    protected List<MoveMove> getAdjacentOpponents(int x, int y, Color color)
    {
        List<MoveMove> result = new ArrayList<>();
        Color opponent = Player.getOpponent(color);
        
        for (int dx = x-1; dx <= x+1; dx++)
        {
            for (int dy = y-1; dy <= y+1; dy++)
            {
                if(isValid(dx, dy) && state[dx][dy] == opponent)
                {
                    result.add(new CrusadeMove(x, y, dx, dy, color));
                }
            }
        }
        return result;
    }
    
    @Override
    public List<Move> getMovesFor(Color color)
    {           
        List<Move> result = new ArrayList<>();
        
        //iterate the whole board to find the player's stones
        for (int x = 0; x < getSize(); x++)
        {
            for (int y = 0; y < getSize(); y++)
            {
                if(state[x][y] == color)
                {
                    //find moves of this player
                    //- all adjacent opponents
                    result.addAll(getAdjacentOpponents(x, y, color));
                }
            }
        }
        return result;
    }

    //TODO remove this method from interface
	//because it only generates problems
	@Override
    public void undoMove(Move move) {
        CrusadeMove m = (CrusadeMove) move;
		
		if(previousMoves.isEmpty() || !getLastMove().equals(m))
		{
			throw new IllegalArgumentException("Cannot undo move which wasn't previously done.");
		}
		
		//TODO check
		state = previousStates.remove(previousStates.size()-1);
		previousMoves.remove(previousMoves.size()-1);
    }

}
