package put.ai.games.crusade;

import put.ai.games.engine.UniversalBoardFactory;

public class CrusadeBoardFactory extends UniversalBoardFactory {

    public CrusadeBoardFactory() throws NoSuchMethodException {
        super(CrusadeBoard.class, "Crusade", "http://homepages.di.fc.ul.pt/~jpn/gv/crusade.htm");
    }
}
