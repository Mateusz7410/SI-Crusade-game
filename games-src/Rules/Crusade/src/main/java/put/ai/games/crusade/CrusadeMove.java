package put.ai.games.crusade;

import put.ai.games.game.Player;
import put.ai.games.game.moves.impl.MoveMoveImpl;

public class CrusadeMove extends MoveMoveImpl {

    public CrusadeMove(int x1, int y1, int x2, int y2, Player.Color color) {
        super(x1, y1, x2, y2, color);
    }

}
