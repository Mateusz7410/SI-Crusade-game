/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package put.ai.games.naiveplayer;

import java.util.List;
import java.util.Random;

import jdk.nashorn.internal.ir.CallNode;
import put.ai.games.game.Board;
import put.ai.games.game.Move;
import put.ai.games.game.Player;

public class NaivePlayer extends Player {
    Board board;
    long availableTime, startTime, elapsedTime, timer;
    int depthTmp;
    int wsp;

    @Override
    public String getName() {
        return "Mateusz Babiaczyk 127325 Mateusz Dąbski 127267";
    }

    @Override
    public Move nextMove(Board b) {
        board = b.clone();
        int bestResult = Integer.MIN_VALUE;
        Move bestMove = null;
        List<Move> moves = board.getMovesFor(getColor());
        int depth = 2;
        availableTime = getTime();
        if(availableTime > 30000)
            timer = 5000;
        else if(availableTime > 15000)
            timer = 2500;
        else if(availableTime < 4000){
            timer = 500;
        }
        else
            timer = 1500;
        startTime = System.currentTimeMillis();

        do {
            for (Move move : moves) {
                wsp = 0;
                if(moves.size() == 1){
                    return move;
                }
                board.doMove(move);
                int result = -alfaBeta(move, depth, Integer.MIN_VALUE, Integer.MAX_VALUE,-1);
                board.undoMove(move);
                result = result + wsp;
                if (result > bestResult) {
                    bestResult = result;
                    bestMove = move;
                }
            }
            depth = depth + 1;
            depthTmp = depth;
            elapsedTime = System.currentTimeMillis() - startTime;
        }while(availableTime - elapsedTime>timer);
        return bestMove;
    }

    public int alfaBeta(Move move, int depth, int alpha, int beta, int color) {
        elapsedTime = System.currentTimeMillis() - startTime;
        if(availableTime - elapsedTime<=timer){
            return 0;
        }

        if(depth <= 0){
            return eval(depth, color);
        }

        List<Move> moves;
        if(color == -1) {
            moves = board.getMovesFor(getOpponent(getColor()));
        }
        else{
            moves = board.getMovesFor(getColor());
        }

        if(moves.isEmpty()){
            return eval(depth, color);
        }


        int bestValue = Integer.MIN_VALUE;

        for (Move currentMove : moves) {
            board.doMove(currentMove);
            int val = -alfaBeta(currentMove,depth - 1, -beta, -alpha, -color);
            board.undoMove(currentMove);

            if (val > bestValue) {
                bestValue = val;
            }

            if (bestValue > alpha) {
                alpha = bestValue;
            }

            if (bestValue >= beta) {
                break;
            }
        }
        return alpha;
    }

    public int eval(int depth, int color){
        int val1 = countStones(getColor());
        int val2 = countStones(getOpponent(getColor()));
        if(board.getWinner(getColor()) != null) {
            if(board.getWinner(getColor()).equals(getColor())){
                //wsp = wsp + 1000;
                if(color == 1)
                    return 100000*(depthTmp-depth);
                else
                    return -100000*(depthTmp-depth);
            }
            if(board.getWinner(getColor()).equals(getOpponent(getColor()))){
                if(color == 1)
                    return -100000*(depthTmp-depth);
                else
                    return 100000*(depthTmp-depth);
            }
        }
        if(val2==0){
            if(color == 1)
                return 10000*(depthTmp-depth);
            else
                return -10000*(depthTmp-depth);
        }
        if(val1==0){
            if(color == 1)
                return -10000*(depthTmp-depth);
            else
                return 10000*(depthTmp-depth);
        }



        double div = val1/val2;
        if(div < 1){
            div = -div;
        }

        int intDiv = (int) Math.round(div*1000);
        int intDiv2 = (int) Math.round(div*10);
        if(div>=1)
            wsp = wsp + intDiv2;
        if(color == 1)
            return intDiv*(depthTmp-depth);
        else
            return -intDiv*(depthTmp-depth);
    }


    public int countStones(Color player)
    {
        int count = 0;

        for (int x = 0; x < board.getSize(); x++)
        {
            for (int y = 0; y < board.getSize(); y++)
            {
                if(board.getState(x,y) == player)
                {
                    count++;
                }
            }
        }

        return count;
    }
}
